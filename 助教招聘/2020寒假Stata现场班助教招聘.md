
![2020寒假Stata现场班-海报-横版](https://file.lianxh.cn/images/20191111/6913c0874f78fc6f176876b66999c401.jpg "2020寒假Stata现场班-海报-横版")

「[2020寒假Stata现场班](http://www.peixun.net/view/1224.html)」（[微信版](https://mp.weixin.qq.com/s/Yg3UMJj1JLlYoz6D-SAGow)）即将开始。为保证本次课程的全方位答疑解惑，现诚邀助教数名，主要辅助老师的教学和答疑工作。助教可以免费参加此次培训课程。

>#### 具体说明和要求：

- **名额：** 9 名 (初级、高级和论文班各 3 名)
- **任务：**
  - **A. 课前准备**：协助完成 3 篇介绍 Stata 和计量经济学基础知识的文档；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；
  - **D. 课程总结**：撰写课程总结推文 (初级、高级和论文班各一篇)
  - Note: 下午 5:00-5:30 的现场答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。
- **截止时间：** 2019 年 11 月 27 日 (将于 2019.11.29 日公布遴选结果)

> **课程主页：**  http://www.peixun.net/view/1224.html

&emsp;

>#### 申请方式：在线提交表单

助教申请网址：https://www.wjx.cn/jq/49566632.aspx

亦可扫码填写助教申请资料：

![连享会-2020寒假Stata现场班助教招聘-扫码填写资料](https://file.lianxh.cn/images/20191111/1f0db45ee0a8fb78141e056e74768fc8.png "连享会-2020寒假Stata现场班助教招聘")

&emsp;

### 附：2019 暑期 Stata 现场班课程简介
---
> #### 完整的知识架构是长期成长的核心动力！

---
## A. 课程概要


> **时间：** 2020 年 1 月 8-17 日    
> **地点：** 北京 中国青年政治学院      
> **授课教师：** 连玉君 (初级+高级) || 江艇 (论文班) 

- **Stata寒假研讨[全程班](http://www.peixun.net/view/1224.html)** 
   - 时间地点：2020 年 1 月 8-17 日，北京
   - 课程链接：[http://www.peixun.net/view/1224.html](http://www.peixun.net/view/1224.html)
- **Stata寒假研讨[初级班](http://www.peixun.net/view/308_detail.html)**
   - 时间地点：2020 年 1 月 8-10 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/307_detail.html](http://www.peixun.net/view/307_detail.html)
- **Stata寒假研讨[高级班](http://www.peixun.net/view/308_detail.html)**
   - 时间地点：2020 年 1 月 12-14 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/308_detail.html](http://www.peixun.net/view/308_detail.html)
- **Stata寒假研讨[论文班](http://www.peixun.net/view/1135.html)**
   - 时间地点：2020 年 1 月 15-17 日 (三天)，北京
   - 课程链接：[http://www.peixun.net/view/1135.html](http://www.peixun.net/view/1135.html)

> [课程介绍 - 微信版](https://mp.weixin.qq.com/s/Yg3UMJj1JLlYoz6D-SAGow)

&emsp;

---

## B. 讲授嘉宾简介

![连玉君](https://images.gitee.com/uploads/images/2019/0801/231203_00d532e3_1522177.jpeg)   
**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，副教授，博士生导师。2007年7月毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为“金融计量”、“计量分析与Stata应用”、“实证金融”等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文 60 余篇。连玉君副教授主持国家自然科学基金项目（2 项）、教育部人文社科基金项目、广东自然科学基金项目等课题项目10余项。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `xtbalance`, `winsor2`, `bdiff`, `hausmanxt`, `ttable3`, `hhi5`, `ua`等。连玉君老师团队一直积极分享Stata应用中的点点滴滴，开设了 [[Stata连享会-简书]](https://www.jianshu.com/u/69a30474ef33)，[[Stata连享会-知乎]](https://www.zhihu.com/people/arlionn) 两个专栏，并定期在微信公众号 (**StataChina**) 中发布精彩推文

---
![江艇](https://images.gitee.com/uploads/images/2019/0801/231203_deb25fc0_1522177.png)   
**江艇**，香港科技大学商学院经济学博士，中国人民大学经济学院副教授，人大国家发展与战略研究院研究员，人大微观数据与实证方法研究中心副主任，美国哥伦比亚大学商学院访问学者。主要研究领域为经济增长与发展、城市经济学、新政治经济学，在 **Economics Letters**、**Review of Development Economics**、《经济研究》、《管理世界》、《世界经济》等国内外著名学术刊物上发表多篇论文，曾应邀在多所高校讲授“应用微观计量经济学”短期前沿课程并广受好评。

