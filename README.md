> [连享会](https://www.lianxh.cn)-直播平台上线了！      
> &emsp;         
> <http://lianxh.duanshu.com>      
> &emsp;         
<img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

> 支持回看，往期精彩课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| :--- | :--- | :--- |
| 空间计量 | 范巧    | 已上线， [T1. 空间计量全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e) |
|     |     | 3月12日，[T2. 地理加权回归模型(GWR)](https://lianxh.duanshu.com/#/brief/course/a62ca9dcb276496097922a61fcf1a701) |
|     | [T2-T5全程](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)      | 3月21日，[T3. 内生时空权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) |
|     |     | 4月05日，[T4. 空间面板模型及动态设定](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3) |
|     |     | 4月11日，[T5. 双重差分空间计量模型(SDID)](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 论文重现 | 连玉君    | 3月9-10日, [经典论文精讲](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)   |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e) &emsp; [静态面板模型-Free](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)    |
| 大数据 | 李兵 | [经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26) |
| R 语言 | 游万海 | [R 语言初识](https://lianxh.duanshu.com/#/brief/course/a719037536de4812a630a599f8cd7b43), 9.9元
| 文本/爬虫 | 游万海<br>司继春 | [4天直播-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html), 2020.3.28-29日; 4.4-5日 |

> Note: 课程详情可以前往 [连享会-课程主页](https://www.lianxh.cn/news/46917f1076104.html) 查看。

 




#### 课程特色
- **深入浅出**：掌握最主流的实证分析方法，获取最新重现代码和文档
- **电子板书**：全程电子板书，课后分享；听课更专注，复习更高效。
- **电子讲义**：分享全套电子版课件 (数据、程序和论文)，课程中的方法和代码都可以快速移植到自己的论文中。

#### 温馨提示

- **听课方式：** 可以通过手机、iPad、电脑在线观看、学习。附带 6 篇 Top 期刊的 Stata 实现过程，涉及 PSM+DID，RDD 等方法。
- **课程咨询：** 李老师-18636102467（微信同号）
- **课件：** 报名后点击课程主页中的「**课程表**」查看。







&emsp;

> Stata连享会 &ensp;   [计量专题](https://www.lianxh.cn/news/46917f1076104.html)  || [直播视频](http://lianxh.duanshu.com) || [知乎推文](https://www.zhihu.com/people/arlionn/) 

![](https://images.gitee.com/uploads/images/2020/0310/085350_090e2186_1522177.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。

