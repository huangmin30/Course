# 2019金秋十月-空间计量专题研讨班课程大纲



## 1\. 课程概览

- **时间：** 2019 年10 月 24-27 (周四-周日)
- **地点：** 电子科技大学沙河校区 (成都)
- **主讲嘉宾**：杨海生 (中山大学)
- **授课方式：**
  - Jupyter-Notebook
  - 每天 6 小时 (9:00-12:00；14:00-17:00)+半小时答疑
  - 讲义电子版于开课前一周发送；打印版由主办方统一印制
  - 软件：Matlab + Stata (配合 Jupyter Notebook)
- [诚聘课程助教 (6名)](https://www.wjx.cn/jq/43524382.aspx)，截止时间：2019年8月31日

&emsp;

## 2\. 主讲嘉宾简介

![主讲嘉宾-杨海生.jpg](https://images.gitee.com/uploads/images/2019/0728/220516_14d6c871_1522177.jpeg "连享会-空间计量专题-主讲嘉宾-杨海生")

[杨海生](http://www.lingnan.sysu.edu.cn/lnshizi/faculty_vch.asp?tn=95)，中山大学岭南学院经济学系副教授，主要研究领域为空间计量经济学理论与应用、实证金融。在 Economic Geography、Ecological Economics、《经济研究》、《管理世界》、《经济学（季刊）》、《管理科学学报》、《金融研究》等学术刊物上发表多篇论文，主持和参与多项国家自然科学基金、广东省自然科学基金等课题研究。

---

## 3\. 课程详情

### 3.1 课程介绍

我们将通过为期四天的讲解，帮助学员们了解并掌握空间相关性的产生缘由、基本的空间计量模型的设定、估计以及相应的实证应用。

具体而言，我们将回顾三代空间计量模型的由来、发展及演变历程。不仅会介绍不同空间模型的设定及其相应的估计方法，而且也将讨论使用这些模型的基本目的及如何解释这些结果。

课程的内容设置以空间计量模型的应用为导向。我们将利用经典文献的数据集对不同的空间计量模型进行演示，并提供分析结果所使用的 **Stata** 和 **Matlab** 程序，以便学员们能够使用这些程序来进行自己的实证分析。此外，我们还将介绍基本模型的拓展及其在金融、国际贸易、环境经济学、财政以及区域经济学等领域中的应用。

各个专题的具体介绍如下：

**第一讲**  首先回顾三代空间计量模型的设定思想和发展演变历程，以及不同空间模型之间的差异，以便各位能够合理设定和筛选模型；进而讲解空间计量模型中的三种主要估计方法：**MLE、IV** 和 **GMM** 区别与联系。

**第二讲**  将在第一讲的基础上引导学员对 **空间权重矩阵** 的设定进行扩展，并讲解如何选择空间模型（即对 **SEM，SAR，SAC，SLX，SDM，SDEM** 模型设定的含义及检验进行剖析）。其次，课程将以 Anselin(1988) 对犯罪率的研究和 Stata manual 中的数据为例，引导学员们如何利用Stata 和 Matlab 估计截面空间模型，并做相应的检验。最后，详细剖析空间模型估计结果的经济学意义及实证结果的展现方式。

**第三讲**  介绍 **高阶空间滞后模型**。包含高阶空间滞后项的空间计量模型的空间权重设定、IV 及 GMM 估计原理和 Matlab + Stata 实现，以及高阶空间滞后模型的拓展及发展方向。

**第四讲**  介绍 **空间联立方程模型** 的由来，并结合 Kelejian and Prucha(2004) 的论文帮助学员们理解在 Stata 中如何对空间联立方程进行设定和估计。此外，课程还将结合 Chen, et al.  (2015) 的论文讲解空间联立方程如何在金融、国际贸易、财政及区域经济学进行拓展。

**第五、六讲**  针对学员们普遍关心（也是目前空间计量应用中最为常见的数据结构）面板数据，本课程将会引导学员使用 Matlab 和 Stata 两种软件对 **静态空间面板** 及 **动态空间面板** 模型进行估计，并对估计结果进行讲解。此外，课程还将通过案例数据针对静态和动态空间面板经常出现的问题进行讲解和解惑。

**第七讲**  介绍 **空间 Probit** 和 **空间 Tobit** 模型。内容包括：当被解释变量为 **二元选择** 或者 **截断数据** 时，空间计量模型应该如何建立和估计。这是近年来空间计量领域的研究热点和难点。我们将结合 Elhorst, et al. (2017) 做实际应用演示。

**第八讲**  将通过一个热门话题 (**同行效应**) 的深入剖析来理解经典空间计量模型在金融领域特别是实证金融中的应用，以便各位了解从 **“讲故事”** 到 **“建模型”** 这一过程中可能遇到的各种障碍和解决方法。首先，我们会通过对 Peer Effect 的刻画，把空间计量模型的应用扩展到实证金融的诸多领域，然后讲解 Peer Effect 中公司之间交互影响的微观理论基础。进而介绍 Peer Effect 的识别问题和参数估计 Stata+Matlab 实现。

--- 

### 3.2 课程大纲

#### 第 1 讲 何谓空间计量？（3 小时）
  - 空间计量模型的由来、发展及演变历程
  - 空间权重矩阵的设定及实现
  - 几类主要的空间计量模型：SEM，SAR，SAC，SLX，SDM，SDEM
  - 空间计量模型的估计方法：MLE、IV 和 GMM

#### 第 2 讲 截面空间计量模型的扩展（3 小时）
  - 空间权重矩阵的扩展及实现
  - 空间计量模型选择及检验
  - 空间计量模型的估计结果的解释及展现形式
  - 如何克服空间权重矩阵内生性
  - 空间计量模型的 Matlab +stata实现
    - **案例1**：Anselin (1988) 犯罪率和家庭收入的截面空间实证模型（Matlab+Stata）
    - **案例2**：Stata manual 空间工具箱应用
    - **案例3**：Xi and Lee (2015) Estimating a spatial autoregressive model with an endogenous spatial weight matrix, JoE, Vol.184:209-232.

#### 第 3 讲 高阶空间滞后模型（3小时）
  - 高阶空间滞后模型的由来（Case et al., 1993）
  - 高阶空间滞后模型的估计（IV 和 GMM）
  - 高阶空间滞后模型的 Matlab +stata实现及应用

#### 第 4 讲 空间联立方程模型（3小时）
  - 空间联立方程的由来
  - 空间联立方程的估计
  - 空间联立方程的  stata实现及应用
  - **案例讲解**：Harry H. Kelejian, Ingmar R. Prucha，“Estimation of Simultaneous Systems of Spatially Interrelated Cross Sectional Equations”JoE，2004，Vol.118 ：27 – 50

#### 第 5-6 讲 空间面板数据模型（6 小时）

  - 空间面板数据模型简介
  - 有固定效应的静态面板模型的估计
    - **案例1:** Baltagi, BH, Li, D(2004) “Prediction in the panel data model with spatial autocorrelation.（matlab）
    - **案例1:** Stata manual 空间工具箱应用
  - 动态面板模型
    - **案例1:** Baltagi, BH, Li, D(2004) “Prediction in the panel data model with spatial autocorrelation.（matlab）
    - **案例2:** Stata Journal Volume 17 Number 1: pp. 139-180 Spatial panel-data models using Stata

#### 第 7 讲 离散和受限数据的空间计量模型（3 小时）

  - Spatial Probit 模型估计原理及 Matlab 实现
    - **案例1:** J.PAUL ELHORST, et.al, 2017，“TRANSITIONS AT DIFFERENT MOMENTS IN TIME: A SPATIAL PROBIT APPROACH”，J. Appl. Econ. 32: 422–439
  - Spatial Tobit 模型估计原理及Matlab实现

#### 第 8 讲 实例剖析：Peer Effect (同群效应)（3 小时）

  - Peer Effect 与空间计量：由来与设定
  - 资产定价中的 Peer Effect (stata+matlab)
  - 公司决策中的 Peer Effect (stata)
  - **范例论文**：3 篇
    - Pirinsky C , Wang Q . Does Corporate Headquarters Location Matter for Stock Returns? [J]. **Journal of Finance**, 2006, 61(4):1991-2015. [[PDF]](https://onlinelibrary.wiley.com/doi/pdf/10.1111/j.1540-6261.2006.00895.x)
    - Leary M T, Roberts M R. Do Peer Firms Affect Corporate Financial Policy?. **Journal of Finance**, 2014, 69(1):139–178. [[PDF]](http://finance.wharton.upenn.edu/~mrrobert/resources/Publications/PeerEffectsJF2014.pdf)
    - Foucault, T., L. Fresard, Learning from peers' stock prices and corporate investment, **Journal of Financial Economics**, 2014, 111 (3): 554-577. [[PDF]](https://ac.els-cdn.com/S0304405X1300295X/1-s2.0-S0304405X1300295X-main.pdf?_tid=8a0270c1-0029-428f-b2a4-d5d7f94ae424&acdnat=1552441238_3b9e1b4b6e1d12c6952f4ff382df1ec2)

&emsp;

### 参考文献：

1. J. Paul Elhorst . Spatial Econometrics: From Cross-Sectional Data to Spatial Panels[M]. Springer Berlin Heidelberg, 2014.
2. Pirinsky C , Wang Q . Does Corporate Headquarters Location Matter for Stock Returns?[J]. The Journal of Finance, 2006, 61(4):1991-2015. [[PDF]](https://onlinelibrary.wiley.com/doi/pdf/10.1111/j.1540-6261.2006.00895.x)
3. Foucault T , Frésard, Laurent. Learning from Peers' Stock Prices and Corporate Investment[J]. Journal of Financial Economics, 2014, 111(3):554-577.
4. Leary M T, Roberts M R. Do Peer Firms Affect Corporate Financial Policy?. **Journal of Finance**, 2014, 69(1):139–178. [[PDF]](http://finance.wharton.upenn.edu/~mrrobert/resources/Publications/PeerEffectsJF2014.pdf)
5. Jillian Grennan . Dividend payments as a response to peer influence. **Journal of Financial Economics**, 2019, 131(3): 549-570. [[PDF]](https://www.sciencedirect.com/science/article/pii/S0304405X18302757)
6. Herskovic B. Networks in Production: Asset Pricing Implications[J]. Journal of Finance, 2018,74(4):1785-1818.
7. Richmond R J . Trade Network Centrality and Currency Risk Premia[J]. Social Science Electronic Publishing, 2015.
8. Barrot J N , Sauvagnat J . Input Specificity and the Propagation of Idiosyncratic Shocks in Production Networks[J]. Quarterly Journal of Economics, 2014(3).
9. Pesaran M H , Yang C F . Econometric Analysis of Production Networks with Dominant Units[J]. Journal of Econometrics Forthcoming., 2016.
10. Yang, Zhenlin. -estimation of fixed-effects spatial dynamic models with short panels[J]. Journal of Econometrics, 2018:S0304407618300599.
11. Lee L F , Yu J . A spatial dynamic panel data model with both time and individual fixed effects [J]. Econometric Theory, 2010, 26(02):564.
12. Prucha K I R . A Generalized Moments Estimator for the Autoregressive Parameter in a Spatial Model[J]. International Economic Review, 1999, 40(2):509-533.
13. Kelejian H H , Prucha I R . Specification and estimation of spatial autoregressive models with autoregressive and heteroskedastic disturbances[J]. Journal of Econometrics, 2010, 157(1):53-67.
14. LEE, LungFei. GMM and 2SLS estimation of mixed regressive, spatial autoregressive models[J]. Journal of Econometrics, 2007, 137(2):489-514.
15. Lung-Fei L , Xiaodong L . Efficient GMM estimation of high order spatial autoregressive models with autoregressive disturbances[J]. Econometric Theory, 2010, 26(1):44.
16. Aklin M. Re-exploring the Trade and Environment Nexus Through the Diffusion of Pollution. **Environmental & Resource Economics**, 2016, 64(4): 663-682. [[PDF]](https://link.springer.com/content/pdf/10.1007%2Fs10640-015-9893-1.pdf)
17. Case A C , Rosen H S , Hines J R . Budget spillovers and fiscal policy interdependence: Evidence from the states[J]. Journal of Public Economics, 1993, 52(3):285-307.
18. 白俊红, 蒋伏心. 协同创新、空间关联与区域创新绩效[J]. 经济研究, 2015,50(07):174-187. [[PDF]](http://sxy.njnu.edu.cn/cxjd/uploadfile/201610/20161002221851174.pdf)
19. 龙小宁, 朱艳丽, 蔡伟贤, 李少民. 基于空间计量模型的中国县级政府间税收竞争的实证分析. **经济研究**, 2014(8): 41-53. [[CNKI]](http://www.cnki.com.cn/Article/CJFDTotal-JJYJ201408004.htm), [[PDF]](http://ww2.usc.cuhk.edu.hk/PaperCollection/webmanager/wkfiles/2012/201506_07_paper.pdf)
20. 尹恒, 徐琰超. 地市级地区问基本建设公共支出的相互影响. **经济研究**, 2011(7): 55-64. [[PDF]](http://ww2.usc.cuhk.edu.hk/PaperCollection/webmanager/wkfiles/8427_1_paper.pdf)
21. 宋马林,金培振.地方保护、资源错配与环境福利绩效[J].经济研究,2016,51(12):47-61.
22. 武红. 中国省域碳减排:时空格局、演变机理及政策建议——基于空间计量经济学的理论与方法[J]. 管理世界, 2015(11):3-10.
23. 刘京军, 苏楚林. 传染的资金:基于网络结构的基金资金流量及业绩影响研究[J]. 管理世界, 2016(1):54-65.
24. 赵善梅, 吴士炜. 基于空间经济学视角下的我国资本回报率影响因素及其提升路径研究[J]. 管理世界, 2018(2):68-79.
25. Lee, Lung-fei, and Jihai Yu. "Estimation of spatial autoregressive panel data models with fixed effects." Journal of Econometrics 154.2 (2010): 165-185.
26. LeSage, James, and Robert Kelley Pace. Introduction to spatial econometrics. Chapman and Hall/CRC, 2009. [[Link]](https://max.book118.com/html/2018/1230/6104220150001242.shtm)
27. Xu, Xingbai, and Lung-fei Lee. "Maximum likelihood estimation of a spatial autoregressive Tobit model." **Journal of Econometrics** 188.1 (2015): 264-280. [[PDF]](https://ac.els-cdn.com/S0304407615001657/1-s2.0-S0304407615001657-main.pdf?_tid=c31a6aed-f862-4ace-8e99-122ac3e23327&acdnat=1552522928_49c9425f1951c322e32c7b0d45211043)


&emsp;

---
## 4. 报名信息
- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**(含报名费、材料费)，差旅及食宿费自理：
  - 全价：4900元/人
  - 预报名价 (10 月 10 日前报名) 4600元/人
  - 团报价：4400元/人（三人及以上）
  - 学生价：4200元/人（报到时需提供学生证原件）
- **老学员优惠：**
  - 参加过一次主办方培训的老学员：4000元/人
  - 参加过两次及以上主办方培训的老学员：3800元/人
- **友情高校感恩优惠：**  
  我们诚挚地感谢曾经团报的高校，来自以下高校的老师和学生可享受感恩价 3900 元/人：(按首字母拼音排序) **安徽财经大学、东北林业大学、河北大学、河北经贸大学、河南财经政法大学、暨南大学、金陵科技学院、南开大学、清华大学、山西财经大学、山西大学、太原科技大学、西安建筑科技大学、西南财经大学、厦门大学、运城学院、中南财经政法大学、中南大学、中山大学、中央财经大学**。
- **Note：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 电话 (微信同号)：王老师 18903405450 ；李老师 ‭18636102467
  - 对公账户：
    - 户名：太原君泉教育咨询有限公司  
    - 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
 
>**温馨提示：** 按报名顺序安排座位
>长按/扫描二维码报\名： http://junquan18903405450.mikecrm.com/0NfnY0Q  
>[![2019金秋十月-空间计量专题报名主页](https://images.gitee.com/uploads/images/2019/0802/115338_d3b8ccc2_1522177.png "2019金秋十月-空间计量专题报名主页")](http://junquan18903405450.mikecrm.com/0NfnY0Q)
&emsp;


## 5. 诚聘助教

- [诚聘课程助教 (6名)](https://www.wjx.cn/jq/43524382.aspx)，截止时间：2019年8月31日
- 报名网址：https://www.wjx.cn/jq/43524382.aspx

> ![助教招聘-2019金秋十月-空间计量专题](https://images.gitee.com/uploads/images/2019/0802/120034_481376d3_1522177.png "连享会-助教招聘-2019-10-成都-空间计量.png")
&emsp;

---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0801/002046_e5a0b681_1522177.jpeg "扫码关注 Stata 连享会")