## 连享会直播：空间计量全局模型及 Matlab 实现

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会直播-空间计量1-范巧-海报竖-700px.png)

### 课程概览

- **听课方式：** 网络直播。报名后点击邀请码进入在线课堂收看，无需安装任何软件。支持手机、iPad、电脑等。
- **直播嘉宾**：范巧 
- **原价**：88 元
- **拼团价**：78元
- **时间**：2020年2月20(周四)，19:00-21:00
- **课件：** 报名后点击短书平台「**课程表**」查看。包括：讲义 PPT，MATLAB 数据和程序文件。
- **课程咨询：** 李老师-18636102467（微信同号）


### 课程提要

- 空间计量全局模型的建模步骤
- 空间权重矩阵的设计、标准化及其 MATLAB 实现
- 模型的参数估计及 MATLAB 实现
- 模型的优选及 MATLAB 实现
- 模型的参数效应分解及 MATLAB 实现


### 课程详情

- **空间计量全局模型通式** 及其向各种退化模型的演化逻辑
- 空间计量全局模型 **建模过程** 中的规范建模步骤
- **空间权重矩阵** 的设计及标准化
  - 空间权重矩阵的设定准则及类型
  - 空间权重矩阵的标准化方法
  - Matlab R2019a 软件中如何实现空间权重矩阵的设计及标准化？
- 空间关系的初步 **模拟** 及 **Moran 指数** 计算
- 空间计量经济学的代表人物、推荐书目及工具箱
- 空间计量模型的 **参数估计及模型优选**
  - 空间计量模型参数估计的 **ML** 及 **MCMC** 方法
  - 8 种经典的空间计量模型 **参数估计及 Matlab 实现** (GNSM、SAC、SDEM、SDM、SEM、SXL、SAR、NSM)
  - **假设检验** 原理、8 种经典的空间计量模型优选及 Matlab 实现（显著性、Wald、LM、LR、Hausman）
- 8 种空间计量模型的 **参数效应分解** 及 Matlab 实现 (直接效应、间接效应及总效应)



### 课程特色

- &#x1F4D9; **深入浅出**：掌握最主流的空间计量全局模型建模及 MATLAB 实现
- &#x1F4D7; **浸入教学**：全程电子板书+ MATLAB操作演示，课后分享电子板书
- &#x1F534; **数据程序**：分享全套课件 (PPT、数据、程序文件)，以便重现课程中演示的所有结果
 

&emsp;

--- 

## 报名

> #### [长按/扫码报名]：
> https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/短书-报名二维码.png)


### 扫码入群：更多分享和讨论

> 亦可直接从海报底部扫描

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/微信群二维码01.png) &emsp; &emsp; &emsp; 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/微信群二维码02.png)

&emsp;

---
## 更多精彩课程 

> 连享会 Live - 直播课主页  &#x1F38F;     
> <https://gitee.com/arlionn/Live>       
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码-连享会直播主页-Live150.png)


&emsp;

---
>#### 关于我们

>[连享会](https://www.lianxh.cn) &ensp; &#x1F34E; [最新专题](https://gitee.com/arlionn/Course/blob/master/README.md) &ensp; &#x1F34F; [直播](https://gitee.com/arlionn/Live) 

[![&#x1F4D7; 点我 - 更多推文](https://file.lianxh.cn/images/20191111/3ed0c73d48c0046f04c502458b4e1c0b.png)](https://gitee.com/arlionn/Course/blob/master/README.md)



- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- **欢迎赐稿：** 欢迎赐稿至StataChina@163.com。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **往期精彩推文：**
 [Stata绘图](https://mp.weixin.qq.com/s/xao8knOk0ulGfNc7vasfew) | [时间序列+面板数据](https://mp.weixin.qq.com/s/8yP1Dijylgreg59QIkqnMg) | [Stata资源](https://mp.weixin.qq.com/s/Kdeoi5uJyNtwwwptdQDQDQ) | [数据处理+程序](https://mp.weixin.qq.com/s/_3DQacFyy7juRjgFedp9WQ) |  [回归分析-交乘项-内生性](https://mp.weixin.qq.com/s/61qJNWnL4KRp0fbLxuDGww)

---

![欢迎加入Stata连享会(公众号: StataChina)](https://file.lianxh.cn/images/20191111/ec83ed2baf9c93494e4f71c9b0f5d766.png)

